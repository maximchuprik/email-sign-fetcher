import mysql.connector as mysql

def sql_connect():

    #client = mysql.connect(host="127.0.0.1", port="3306", user="root", passwd="root", db="email")
    client = mysql.connect(host="127.0.0.1", port="3306", user="root", passwd="root", db="newEmail")

    try:
        return client
    except:
        return False

def select():

    client = sql_connect()

    try:
        cursor = client.cursor()
        #qeury = "SELECT id, body, subject, `from` FROM sq_inbox WHERE id = 6"
        qeury = "SELECT id, body, subject, `from` FROM sq_inbox"
        cursor.execute(qeury)
        results = cursor.fetchall()

        return results
    finally:
        client.close()


        # python console/email_finder.py > ~/sites/temp/EmailFinder_Test