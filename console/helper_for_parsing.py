import re
from lxml import html, etree

# Location
pattern_address_abbr = r'\bCanada\b|\bCreek\b|\bRd\b|\bALY\b|\bALLEY\b|\bANX\b|\bANNEX\b|\bAPT\b|\bAPARTMENT\b|\bARC\b|\bARCADE\b|\bAVE\b|\bAVENUE\b|\bBSMT\b|\bBASEMENT\b|\bBYU\b|\bBAYOU\b|\bBCH\b|\bBEACH\b|\bBND\b|\bBEND\b|\bBLF\b|\bBLUFF\b|\bBTM\b|\bBOTTOM\b|\bBLVD\b|\bBOULEVARD\b|\bBR\b|\bBRANCH\b|\bBRG\b|\bBRIDGE\b|\bBRK\b|\bBROOK\b|\bBLDG\b|\bBUILDING\b|\bBG\b|\bBURG\b|\bBYP\b|\bBYPASS\b|\bCP\b|\bCAMP\b|\bCYN\b|\bCANYON\b|\bCPE\b|\bCAPE\b|\bCSWY\b|\bCAUSEWAY\b|\bCTR\b|\bCENTER\b|\bCIR\b|\bCIRCLE\b|\bCLFS\b|\bCLIFF\b|\bCLFS\b|\bCLIFFS\b|\bCLB\b|\bCLUB\b|\bCOR\b|\bCORNER\b|\bCORS\b|\bCORNERS\b|\bCRSE\b|\bCOURSE\b|\bCT\b|\bCOURT\b|\bCTS\b|\bCOURTS\b|\bCV\b|\bCOVE\b|\bCRK\b|\bCREEK\b|\bCRES\b|\bCRESCENT\b|\bXING\b|\bCROSSING\b|\bDL\b|\bDALE\b|\bDM\b|\bDAM\b|\bDEPT\b|\bDEPARTMENT\b|\bDV\b|\bDIVIDE\b|\bDR\b|\bDRIVE\b|\bEST\b|\bESTATE\b|\bEXPY\b|\bEXPRESSWAY\b|\bEXT\b|\bEXTENSION\b|\bFLS\b|\bFALLS\b|\bFRY\b|\bFERRY\b|\bFLD\b|\bFIELD\b|\bFLDS\b|\bFIELDS\b|\bFLT\b|\bFLAT\b|\bFL\b|\bFLOOR\b|\bFRD\b|\bFORD\b|\bFRST\b|\bFOREST\b|\bFRG\b|\bFORGE\b|\bFRK\b|\bFORK\b|\bFRKS\b|\bFORKS\b|\bFT\b|\bFORT\b|\bFWY\b|\bFREEWAY\b|\bFRNT\b|\bFRONT\b|\bGDNS\b|\bGARDEN\b|\bGDNS\b|\bGARDENS\b|\bGTWY\b|\bGATEWAY\b|\bGLN\b|\bGLEN\b|\bGRN\b|\bGREEN\b|\bGRV\b|\bGROVE\b|\bHNGR\b|\bHANGER\b|\bHBR\b|\bHARBOR\b|\bHVN\b|\bHAVEN\b|\bHTS\b|\bHEIGHTS\b|\bHWY\b|\bHIGHWAY\b|\bHL\b|\bHILL\b|\bHLS\b|\bHILLS\b|\bHOLW\b|\bHOLLOW\b|\bINLT\b|\bINLET\b|\bIS\b|\bISLAND\b|\bISS\b|\bISLANDS\b|\bJCT\b|\bJUNCTION\b|\bKY\b|\bKEY\b|\bKNLS\b|\bKNOLL\b|\bKNLS\b|\bKNOLLS\b|\bLK\b|\bLAKE\b|\bLKS\b|\bLAKES\b|\bLNDG\b|\bLANDING\b|\bLN\b|\bLANE\b|\bLGT\b|\bLIGHT\b|\bLF\b|\bLOAF\b|\bLBBY\b|\bLOBBY\b|\bLCKS\b|\bLOCK\b|\bLCKS\b|\bLOCKS\b|\bLDG\b|\bLODGE\b|\bLOWR\b|\bLOWER\b|\bMNR\b|\bMANOR\b|\bMDWS\b|\bMEADOW\b|\bMDWS\b|\bMEADOWS\b|\bML\b|\bMILL\b|\bMLS\b|\bMILLS\b|\bMSN\b|\bMISSION\b|\bMT\b|\bMOUNT\b|\bMTN\b|\bMOUNTAIN\b|\bNCK\b|\bNECK\b|\bOFC\b|\bOFFICE\b|\bORCH\b|\bORCHARD\b|\bPKWY\b|\bPARKWAY\b|\bPH\b|\bPENTHOUSE\b|\bPNES\b|\bPINE\b|\bPNES\b|\bPINES\b|\bPL\b|\bPLACE\b|\bPLN\b|\bPLAIN\b|\bPLNS\b|\bPLAINS\b|\bPLZ\b|\bPLAZA\b|\bPT\b|\bPOINT\b|\bPRT\b|\bPORT\b|\bPR\b|\bPRAIRIE\b|\bRADL\b|\bRADIAL\b|\bRNCH\b|\bRANCH\b|\bRPDS\b|\bRAPID\b|\bRPDS\b|\bRAPIDS\b|\bRST\b|\bREST\b|\bRDG\b|\bRIDGE\b|\bRIV\b|\bRIVER\b|\bRD\b|\bROAD\b|\bRM\b|\bROOM\b|\bSHL\b|\bSHOAL\b|\bSHLS\b|\bSHOALS\b|\bSHR\b|\bSHORE\b|\bSHRS\b|\bSHORES\b|\bSPC\b|\bSPACE\b|\bSPG\b|\bSPRING\b|\bSPGS\b|\bSPRINGS\b|\bSQ\b|\bSQUARE\b|\bSTA\b|\bSTATION\b|\bSTRA\b|\bSTRAVENUE\b|\bSTRM\b|\bSTREAM\b|\bST\b|\bSTREET\b|\bSTE\b|\bSUITE\b|\bSMT\b|\bSUMMIT\b|\bTER\b|\bTERRACE\b|\bTRCE\b|\bTRACE\b|\bTRAK\b|\bTRACK\b|\bTRFY\b|\bTRAFFICWAY\b|\bTRL\b|\bTRAIL\b|\bTRLR\b|\bTRAILER\b|\bTUNL\b|\bTUNNEL\b|\bTPKE\b|\bTURNPIKE\b|\bUN\b|\bUNION\b|\bUPPR\b|\bUPPER\b|\bVLY\b|\bVALLEY\b|\bVIA\b|\bVIADUCT\b|\bVW\b|\bVIEW\b|\bVLG\b|\bVILLAGE\b|\bVL\b|\bVILLE\b|\bVIS\b|\bVISTA\b|\bWAY\b|\bWAY\b|\bWLS\b|\bWELL\b|\bWLS\b|\bWELLS\b|\bAVENIDA\b|\bAVE\b|\bCALLE\b|\bCLL\b|\bCAMINITO\b|\bCMT\b|\bCAMINO\b|\bCAM\b|\bCERRADA\b|\bCER\b|\bCIRCULO\b|\bCIR\b|\bENTRADA\b|\bENT\b|\bPASEO\b|\bPSO\b|\bPLACITA\b|\bPLA\b|\bRANCHO\b|\bRCH\b|\bVEREDA\b|\bVER\b|\bVISTA\b|\bVIS\b|Adel\sGade|Annas\sFancy|Annas\sRetreat|Bjerge\sGade|Castle\sCoakley|Estate\sBovoni|Holgers\sHope|King\sCross\sSt|Long\sPt|Lower\sHull|Lower\sLerkenlund|Lytton\sFancy|Mahogany\sWelcome|Mount\sPleasant|New\sSt|Oldenney|Paradise\sMls'
pattern_state_abbr = r'\bAA\b|\bAE\b|\bAK\b|\bAL\b|\bAP\b|\bAR\b|\bAS\b|\bAZ\b|\bCA\b|\bCO\b|\bCT\b|\bDC\b|\bDE\b|\bFL\b|\bFM\b|\bGA\b|\bGU\b|\bHI\b|\bIA\b|\bID\b|\bIL\b|\bIN\b|\bKS\b|\bKY\b|\bLA\b|\bMA\b|\bMD\b|\bME\b|\bMH\b|\bMI\b|\bMN\b|\bMO\b|\bMP\b|\bMS\b|\bMT\b|\bNC\b|\bND\b|\bNE\b|\bNH\b|\bNJ\b|\bNM\b|\bNV\b|\bNY\b|\bOH\b|\bOK\b|\bOR\b|\bPA\b|\bPR\b|\bPW\b|\bRI\b|\bSC\b|\bSD\b|\bTN\b|\bTX\b|\bUT\b|\bVA\b|\bVI\b|\bVT\b|\bWA\b|\bWI\b|\bWV\b|\bWY\b|Armed\sForces\sAmericas|Armed\sForces\sEurope|Alaska|Alabama|Armed\sForces\sPacific|Arkansas|American\sSomoa|Arizona|California|Colorado|Connecticut|District\sof\sColumbia|Delaware|Florida|Federated\sStates\sof\sMicronesia|Georgia|Guam|Hawaii|Iowa|Idaho|Illinois|Indiana|Kansas|Kentucky|Louisiana|Massachusetts|Maryland|Maine|Marshall\sIslands|Michigan|Minnesota|Missouri|Northern\sMariana\sIslands|Mississippi|Montana|North\sCarolina|North\sDakota|Nebraska|New\sHampshire|New\sJersey|New\sMexico|Nevada|New\sYork|Ohio|Oklahoma|Oregon|Pennsylvania|Puerto\sRico|Palau|Rhode\sIsland|South\sCarolina|South\sDakota|Tennessee|Texas|Utah|Virginia|Virgin\sIslands|Vermont|Washington|Wisconsin|West\sVirginia|Wyoming|\bNSW\b|\bQLD\b|\bSA\b|\bTAS\b|\bVIC\b|\bNT\b|\bACT\b|\bJBT\b|\bON\b'
pattern_city_au = r'\bHorsham\b|\bLakeview\b|\bCairns\b|\bCanberra\b|\bBallarat\b|\bBendigo\b|\bBroadford\b|\bGeelong\b|\bLa\b\s\bTrobe\b|\bMelbourne\b|\bMildura\b|\bSwan\b\s\bHill\b|\bWodonga\b|\bShepparton\b|\bAlbany\b|\bBunbury\b|\bBroome\b|\bGeraldton\b|\bKalgoorlie\b|\bCarnarvon\b|\bLeonora\b|\bMeekatharra\b|\bPerth\b|\bPort\b\s\bHedland\b|\bTom\b\s\bPrice\b|\bWiluna\b|\bEsperance\b|\bBundaberg\b|\bBrisbane\b|\bGympie\b|\bGladstone\b|\bGold\b\s\bCoast\b|\bIpswich\b|\bCaloundra\b|\bMackay\b|\bMount\b\s\bIsa\b|\bMaryborough\b|\bNambour\b|\bRockhampton\b|\bRedcliffe\b|\bSunshine\b\s\bCoast\b|\bTownsville\b|\bToowoomba\b|\bWeipa\b|\bHervey\b\s\bBay\b|\bCharters\b\s\bTowers\b|\bArmidale\b|\bBathurst\b|\bBroken\b\s\bHill\b|\bWyong\b|\bWollongong\b|\bGosford\b|\bGoulburn\b|\bGrafton\b|\bGriffith\b|\bDubbo\b|\bKatoomba\b|\bCoffs\b\s\bHarbour\b|\bQueanbeyan\b|\bLismore\b|\bLake\b\s\bMacquarie\b|\bMaitland\b|\bNowra\b|\bNewcastle\b|\bAlbury\b|\bOrange\b|\bPort\b\s\bMacquarie\b|\bCessnock\b|\bSydney\b|\bTamworth\b|\bTweed\b\s\bHeads\b|\bWagga\b\s\bWagga\b|\bAlice\b\s\bSprings\b|\bDarwin\b|\bPalmerston\b|\bTennant\b\s\bCreek\b|\bBurnie\b|\bDevonport\b|\bLaunceston\b|\bHobart\b|\bAdelaide\b|\bVictor\b\s\bHarbor\b|\bMurray\b\s\bBridge\b|\bMount\b\s\bGambier\b|\bPort\b\s\bLincoln\b|\bPort\b\s\bAugusta\b|\bPort\b\s\bPirie\b|\bWhyalla\b'
pattern_for_country = r'\b(Albania|Andorra|Austria|Byelorussia|Belgium|Bosnia|Bulgaria|Croatia|Czech|Republic|Denmark|Estonia|Finland|France|Germany|Greece|Hungary|Iceland|Ireland|Italy|Latvia|Liechtenstein|Lithuania|Luxemburg|Macedonia|Malta|Moldavia|Monaco|Montenegro|Netherlands|Norway|Poland|Portugal|Rumania|Russia|Serbia|Slovakia|Slovenia|Spain|Sweden|Switzerland|Ukraine|Great\sBritain)\b'

# Phone
# pattern_for_phone = r'\b(((\+\d{1,3}(-|.|\s)?\(?\d\)?(-|\s|.)?\d{1,5})|(\(?\d{2,6}\)?))(-|.|\s)?(\d{3,4})(-|.|\s)?(\d{3,4})((\sx|\sext)\d{1,5}){0,1}|([\d]{4}\s[\d]{2}\s[\d]{2}\s[\d]{2}))\b'
# pattern_for_phone = r'\b(((\+\d{1,3}(\-|\.|\s)?\(?\d\)?(\-|\s|\.)?\d{1,5})|(\(?\d{2,6}\)?))(\-|\.|\s)?(\d{3,4})(\-|\.|\s)?(\d{3,4})(\d{1,5}){0,1}|([\d]{4}\s[\d]{2}\s[\d]{2}\s[\d]{2}))\b'
pattern_for_phone = r'\b(((\+\d{1,3}(\-|\.|\s)?\(?\d\)?(\-|\s|\.)?\d{1,5})|(\(?\d{2,6}\)?))(\-|\.|\s)?(\d{3,4})(\-|\.|\s)?(\d{3,4})(\d{1,5}){0,1}|([\d]{2,4}\s[\d]{2}\s[\d]{2}\s[\d]{2}(\s[\d]{2})?))\b'
pattern_phone_abbr = r'(\bFax|\bPhone|\bP|\bF|\bp|\bf|\bof\b|\bPh\b|\btel\b|\btelepfone\b|\bphone\b|\bfax\b|Address|Contact\s\bUs\b|Contact|Call\sus|LOCATION)(\s)?:'

# IP address
pattern_for_ip = r'\b(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\b'

# Email
pattern_for_email = r'[\+\d\w\.]{2,}@[\d\w.]+\.[\w]{2,}'

# Date
pattern_for_months = r'\b(january|february|march|april|may|june|july|august|september|october|november|december|jan|feb|mar|apr|may|jun|jul|aug|sep|sept|oct|nov|dec|month)\b'
pattern_for_days = r'\b(monday|tuesday|wednesday|thursday|friday|saturday|sunday|mon|tue|tues|wed|thu|thur|thurs|fri|sat|sun)\b'
pattern_for_date = r'\b(\d{4})\d{2}(\d{2})\b'
pattern_for_time = r'\b\d{1,2}:\d{2}(h)?\b'

# User Agent
pattern_for_user_agent = r'User\sAgent'

# Form`s

polite_form = r'((faithfully|cordially)\syours|((warm|best|kind)\s)?(regards|wishes)|cheers|thanks|(thank|thanking)\syou|mit\sfreundlichen)'
header = r'\b(from|sent|to|subject|de|para|cc)\:'


def check_location(location):
    check = 0
    location = re.sub(r'(\b13;|\n)', '', location)
    temp = re.sub(r'(&#\d+(;)?|\b13;)', '', location).strip()
    matches = re.findall(pattern_address_abbr, temp, re.IGNORECASE)

    if not re.search(r'[\d]{1,6}', temp, re.IGNORECASE):
        return False
    if len(temp) > 10:
        check += 1
    if re.search(r',', temp):
        check += 1
    if matches:
        check += len(matches)
    if re.search(pattern_for_phone, temp):
        return False
    if re.search(pattern_for_country, temp, re.IGNORECASE):
        check += 1
    if re.search(pattern_state_abbr, temp, re.IGNORECASE):
        check += 1
    if re.search(pattern_city_au, temp, re.IGNORECASE):
        check += 1
    if re.search(pattern_for_months, temp, re.IGNORECASE):
        return False
    if re.search(pattern_for_days, temp, re.IGNORECASE):
        return False
    if re.search(pattern_for_time, temp, re.IGNORECASE):
        return False
    if re.search(pattern_for_email, temp, re.IGNORECASE):
        return False
    if re.search(pattern_for_user_agent, temp, re.IGNORECASE):
        return False
    if re.search(r'version|(AES(\_|\-)256)', temp, re.IGNORECASE):
        return False
    if check > 2 and len(temp) < 140:
        # print check, "-", location.strip()
        return location.replace("&#183;", ".").strip().replace("<split", "")

    return False


def preparation_letter(letter):
    body = html.fromstring(letter)
    etree.strip_elements(body, 'head', 'link', 'style', 'path', 'script', 'svg', 'symbol', 'img')
    etree.strip_tags(body, 'sup', 'img')
    etree.strip_attributes(body, 'href', 'yahoo', 'style', 'xmlns', 'authenticate', 'display', 'content', 'face', 'dir',
                           'src', 'emloc', 'class', 'id', 'color', 'width', 'align', 'valign', 'target', 'cellpadding',
                           'cellspacing', 'border', 'bgcolor', 'height', 'title', 'type', 'name', 'value', 'colspan',
                           'alt', 'background', 'data-renderer-used', 'data-component-name', 'data-redactor-tag',
                           'data-saferedirecturl', 'data-mce-style', 'data-is-block', 'data-placeholder', 'data-index',
                           'data-content-region-name', 'data-unsubscribe', 'data-smartmail', 'data-hs-link-id',
                           'data-hs-cos-general-type', 'data-verified', 'data-widget-type', 'data-hs-cos-type')
    clear_letter = etree.tostring(body)

    clear_letter = re.sub(r'(<\/[^>]*>|<[^>\_]*>|&gt;)', '', clear_letter)

    clear_letter = re.sub(r'(&#160;){2,}', '', clear_letter)
    clear_letter = clear_letter.replace("&#160;", "")
    clear_letter = re.sub(r'(&#13;){2,}', '&#13;', clear_letter.replace("&#160;", " "))
    # clear_letter = re.sub(r'(\s\/\s|\|)', '&#13;', clear_letter)
    clear_letter = re.sub(r'(\s){2,}', '&#13;', clear_letter)
    clear_letter = re.sub(r'(&#13;\n){2,}', '<split>', clear_letter)

    return clear_letter
