#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import db_connect as mysql
import helper_for_parsing as help
from difflib import SequenceMatcher

for id, result, subject, frm in mysql.select():
    try:
        if frm and result:
            if not re.search(
                    r'(no(t)?(\-|\_|\.)?reply\@|donotreply|\bjob(s)?\B|\breply\@|news\@|\bsales\@|\bbot\@|reports\@|notifications|monitor|daemon|events\@|welcome\@|hello@|info\@|postmaster\@|notify\@|noreplyaddress|\bactivation\@|\broot\@|\balert(s)?\@|\bpromotions\@|\bplease\-reply\@|\btickets\@|\bapp\@|\bsuccess\@|\btoken\@|wordpress\@)',
                    frm, re.IGNORECASE):

                if re.search(r'<(address|[^(\-\-)]{2}[^<>]*\b(ad(dress|r)|loc).)[^<>]*>', result, re.IGNORECASE):
                    print "yes address 100%"

                body = re.sub(r'<table[^<>]*>', '<rep_tab>', result)

                body = help.preparation_letter(body)

                for tab_body in body.split('<rep_tab>'):

                    if re.search(help.polite_form, tab_body, re.IGNORECASE | re.MULTILINE):

                        delimiter = re.sub(help.header, '<sub>', tab_body).split('<sub>')

                        # print delimiter, "\n" * 2

                        for delim in delimiter:

                            new_list = []
                            start_polite_form = re.search(help.polite_form, delim,
                                                          re.IGNORECASE | re.MULTILINE).start()
                            header = re.search(help.header, delim, re.IGNORECASE | re.MULTILINE)
                            if header and header.start() > start_polite_form:
                                start_header = re.search(help.header, delim, re.IGNORECASE | re.MULTILINE).start()
                            else:
                                start_header = -1

                            new_body = delim[start_polite_form:start_header]

                            for letter in new_body.split("<split>"):
                                if len(letter) < 500:
                                    new_list.append(letter)

                            inbox_list = dict()

                            inbx = re.sub(r'@[\d\w.]+\.[\w]{2,}', '', frm)
                            domain_email = re.sub(r'[\d\w.]{2,}@', '', frm)

                            phone_list = []

                            for inbox in "&#13;".join(new_list).split("&#13;"):

                                inbox = inbox.replace("<split", "")

                                if re.search(help.pattern_for_phone, inbox):
                                    phone_list.append(re.search(help.pattern_for_phone, inbox).group())
                                elif re.search(help.pattern_for_email, inbox):
                                    inbox_list['email'] = re.search(help.pattern_for_email, inbox).group()
                                elif help.check_location(inbox):
                                    inbox_list['location'] = help.check_location(inbox)
                                elif round(SequenceMatcher(None, inbox.lower(), inbx.lower()).ratio(),
                                           2) > 0.5 and len(
                                    inbox.strip().split()) > 1 and not re.search(help.pattern_for_email,
                                                                                 inbox) and not re.search(
                                    help.header,
                                    inbox,
                                    re.IGNORECASE):
                                    inbox_list['name'] = inbox

                            inbox_list['phone'] = list(set(phone_list))
                            inbox_list['type'] = "person"

                            print inbox_list
                    else:
                        tab_body = re.sub(r'(\n&#13;){2,}', '<split>', tab_body).replace('&#13;&#13;', '<split>')

                        # print tab_body, "\n" * 2

                        for inbox in tab_body.split("<split>"):
                            if re.search(help.pattern_for_phone, inbox) and not re.search(help.header, inbox):

                                phone_list = []
                                inbox_list = dict()

                                for inbx in inbox.split("&#13;"):
                                    if re.search(help.pattern_for_phone, inbx):
                                        phone_list.append(re.search(help.pattern_for_phone, inbx).group())
                                    elif re.search(help.pattern_for_email, inbx):
                                        inbox_list['email'] = re.search(help.pattern_for_email, inbx).group()
                                    elif help.check_location(inbx):
                                        inbox_list['location'] = help.check_location(inbx)

                                inbox_list['phone'] = phone_list
                                inbox_list['type'] = "info"

                                print inbox_list

                print "=" * 5, "\n" * 2


    except:
        pass
